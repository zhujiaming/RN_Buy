/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ToastAndroid,
    Image,
    Switch,
    TouchableOpacity
} from 'react-native';

var CommonCell = React.createClass({
    getDefaultProps(){
        return {
            title: '',//左边的标题
            rightTextHint: '',//右边的提示小字
            isSwitch: false,//右边是否为开关模式
            onSwitchChange: null,//开关模式监听
            showBottomLine: true,
            showTopLine: false,
            onCellPress: null,//cell点击
            imgUriLeft: null,//左边cell图标
        };
    },
    getInitialState(){
        return {
            isOn: false,
        }
    },
    render() {
        return (
            <TouchableOpacity activeOpacity={0.6} onPress={()=> {
                if (this.props.onCellPress)
                    this.props.onCellPress()
            }}>
                <View style={[styles.container, {
                    borderBottomWidth: this.props.showBottomLine ? 0.5 : 0,
                    borderTopWidth: this.props.showTopLine ? 0.5 : 0,
                }]}>
                    {/*左边*/}
                    {this._renderLeft()}
                    {/*中间*/}
                    <View style={{flex: 1}}/>
                    {/*右边*/}
                    {this._renderRight()}
                </View>
            </TouchableOpacity>
        );
    },
    _renderLeft(){
        return (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {this.props.imgUriLeft ?
                    <Image source={{uri: this.props.imgUriLeft}} style={styles.imgLeftStyle}/> : null}
                <Text style={styles.titleStyle}>{this.props.title}</Text>
            </View>);
    },
    _renderRight(){
        if (this.props.isSwitch) {
            return this._renderSwitch();
        } else {
            return (<View style={styles.rightViewStyle}>
                <Text style={styles.rightTextHintStyle}>{this.props.rightTextHint}</Text>
                <Image source={{uri: 'home_arrow'}} style={{width: 20, height: 25}}/>
            </View>);
        }
    },
    _renderSwitch(){
        return (<Switch value={this.state.isOn} onValueChange={(value)=> {
            this.setState({isOn: value});
            if (this.props.onSwitchChange) {
                this.props.onSwitchChange(value);
            }
        }}/>);
    }
});

const styles = StyleSheet.create({
    container: {
        height: 50,
        backgroundColor: 'white',
        alignItems: 'center',
        borderBottomColor: '#dddddd',
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
        borderTopColor: '#dddddd',
    },
    titleStyle: {
        fontSize: 17,
        color: 'black'
    },
    rightViewStyle: {
        flexDirection: 'row'
    },
    rightTextHintStyle: {
        color: 'gray',
        fontSize: 15,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    imgLeftStyle: {
        width: 30,
        height: 30,
        marginRight: 10,
        borderRadius: 15,
    }

});

//输出组件类
module.exports = CommonCell;

