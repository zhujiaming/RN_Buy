/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';


var TitleBar = React.createClass({
    getDefaultProps(){
        return {
            leftIconUri: '',
            rightIconUri: '',
            middleText: '标题',
            onRightPress: null,
        };
    },
    render() {
        return (
            <View style={styles.topBarStyle}>
                {/*左边*/}
                {this._renderLeft()}
                {/*中间*/}
                {this._renderMiddle()}
                {/*右边*/}
                <TouchableOpacity activeOpacity={0.5} onPress={()=> {
                    this._onRightPress();
                }}>
                    {this._renderRight()}
                </TouchableOpacity>
            </View>
        );
    },
    _renderLeft(){
        var leftIconUri = this.props.leftIconUri;
        if (leftIconUri === 'none') {
            return <View style={styles.topBarImgStyle}/>;
        } else {
            return (
                <TouchableOpacity activeOpacity={0.7} onPress={this._onLeftPress}><Image style={styles.topBarImgStyle}
                                                                                         source={{uri: leftIconUri === '' ? 'back' : leftIconUri}}/></TouchableOpacity>);
        }
    },
    _renderMiddle(){
        return <Text style={styles.topBarMiddleStyle}>{this.props.middleText}</Text>
    },
    _renderRight(){
        var rightIconUri = this.props.rightIconUri;
        if (rightIconUri) {
            return (<Image style={styles.topBarImgStyle}
                           source={{uri: rightIconUri}}/>);
        } else {
            return null;
        }
    },
    _onRightPress(){
        if (this.props.onRightPress) {
            this.props.onRightPress();
        }
    },
    _onLeftPress(){
        if (this.props.navigator)
            this.props.navigator.pop();
    }
});

const styles = StyleSheet.create({
    topBarStyle: {
        flexDirection: 'row',
        backgroundColor: '#FF6100',
        alignItems: 'center',
        height: 49,
        paddingLeft: 5,
        paddingRight: 5,
    },
    topBarImgStyle: {
        width: 23,
        height: 23
    },
    topBarMiddleStyle: {
        color: 'white',
        fontSize: 20,
        flex: 1,
        textAlignVertical: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    }
});

//输出组件类
module.exports = TitleBar;

