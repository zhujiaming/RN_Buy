/**
 * Created by Administrator on 2017/3/1.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Platform,
    Navigator,
    StatusBar,
    BackAndroid
} from 'react-native';

import TabNavigator from 'react-native-tab-navigator';

var Home = require('../Home/XMGHome');
var Shop = require('../Shop/XMGShop');
var Mine = require('../Mine/XMGMine');
var More = require('../More/XMGMore');


var Main = React.createClass({
    getInitialState(){
        return {
            selectedTab: 'home'
        }
    },
    componentWillMount(){
        BackAndroid.addEventListener('backAndroid', ()=> {
                return this._onBackPress();
            }
        );
    },
    componentWillUnmount(){
        BackAndroid.removeEventListener('backAndroid');
    }
    ,
    render() {
        return (
            <TabNavigator>
                {this._renderTabBarItem("首页", 'icon_tabbar_homepage', 'icon_tabbar_homepage_selected', 'home', <Home
                    navigator={this.props.navigator}/>)}
                {this._renderTabBarItem("商家", 'icon_tabbar_merchant_normal', 'icon_tabbar_merchant_selected', 'shop',
                    <Shop navigator={this.props.navigator}/>)}
                {this._renderTabBarItem("我的", 'icon_tabbar_mine', 'icon_tabbar_mine_selected', 'mine', <Mine
                    navigator={this.props.navigator}/>, true)}
                {this._renderTabBarItem("更多", 'icon_tabbar_misc', 'icon_tabbar_misc_selected', 'more', <More
                    navigator={this.props.navigator}/>)}
            </TabNavigator>
        );
    },
    _renderTabBarItem(title, iconUri, iconSelectedUri, tabTag, component, renderBadge){
        return ( <TabNavigator.Item
            title={title}
            renderIcon={() => <Image source={{uri: iconUri}} style={styles.iconStyle}/>}
            renderSelectedIcon={() => <Image source={{uri: iconSelectedUri}}
                                             style={styles.iconStyle}/>}
            onPress={()=> {
                this.setState({selectedTab: tabTag})
            }}
            selected={this.state.selectedTab === tabTag}
            selectedTitleStyle={styles.tabTitleSelectedStyle}
            renderBadge={renderBadge ? this._renderBadge : null}
        >
            {component}
        </TabNavigator.Item>);
    },
    _renderBadge(){
        return (<Text style={styles.badgeStyle}>&hearts;</Text>);
    },
    _onBackPress(){
        var cRoute = this.props.navigator.getCurrentRoutes();
        console.log(cRoute);
        var cRouteNum = cRoute.length;
        if (cRouteNum > 1) {
            this.props.navigator.pop();
            return true;
        } else {
            //退出软件
            return false;
        }
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconStyle: {
        width: Platform === 'ios' ? 30 : 25,
        height: Platform === 'ios' ? 30 : 25,
    },
    tabTitleSelectedStyle: {
        color: '#FF6100'
    },
    badgeStyle: {
        backgroundColor: '#00000000',
        color: 'red',
        paddingRight: 5,
    }


});

//输出组件类
module.exports = Main;

