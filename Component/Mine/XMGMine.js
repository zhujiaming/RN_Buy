/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView
} from 'react-native';

var CommonCell = require('../kit/XMGCommonCell');

var Mine = React.createClass({
    render() {
        return (

            <ScrollView style={styles.container}>
                {this._renderTop()}
                {this._renderMiddle()}
                {this._renderBottomList()}
            </ScrollView>
        );
    },
    _renderMiddle(){
        return (<View>
            <CommonCell title={'我的订单'} showBottomLine={true} showTopLine={true} imgUriLeft={'draft'}
                        rightTextHint={'查看全部订单'}/>
            <View style={{flexDirection: 'row', backgroundColor: 'white', alignItems: 'center'}}>
                <View style={styles.orderViewStyle}>
                    <Image source={{uri: 'order1'}} style={styles.orderimageStyle}></Image>
                    <Text style={styles.orderTextStyle}>待付款</Text>
                </View>

                <View style={styles.orderViewStyle}>
                    <Image source={{uri: 'order2'}} style={styles.orderimageStyle}></Image>
                    <Text style={styles.orderTextStyle}>待使用</Text>
                </View>

                <View style={styles.orderViewStyle}>
                    <Image source={{uri: 'order3'}} style={styles.orderimageStyle}></Image>
                    <Text style={styles.orderTextStyle}>待评价</Text>
                </View>

                <View style={styles.orderViewStyle}>
                    <Image source={{uri: 'order4'}} style={styles.orderimageStyle}></Image>
                    <Text style={styles.orderTextStyle}>退款/售后</Text>
                </View>

            </View>
        </View>);
    },
    _renderBottomList(){
        return (<View>
            <View style={{height: 10}}/>
            <CommonCell title={'小马哥钱包'} showBottomLine={true} showTopLine={true} imgUriLeft={'pay'}
                        rightTextHint={'账户余额:¥100'}/>
            <CommonCell title={'抵用券'} showBottomLine={true} imgUriLeft={'card'}
                        rightTextHint={'0'}/>
            <View style={{height: 10}}/>
            <CommonCell title={'积分商城'} showBottomLine={true} showTopLine={true} imgUriLeft={'like'}/>
            <View style={{height: 10}}/>
            <CommonCell title={'今日推荐'} showBottomLine={true} showTopLine={true} imgUriLeft={'new_friend'}/>
            <View style={{height: 10}}/>
            <CommonCell title={'我要合作'} showBottomLine={true} showTopLine={true} imgUriLeft={'draft'}
                        rightTextHint={'轻松开店,招财进宝'}
            />
        </View>);
    },
    _renderTop(){
        return (<View style={{backgroundColor: '#FF6100'}}>
            <View style={{margin: 10, alignItems: 'center', flexDirection: 'row', marginTop: 40}}>
                <Image source={{uri: 'meinv'}} style={styles.headIconStyle}/>
                <Text style={styles.headUserNameStyle}>美团用户</Text>
                <Image source={{uri: 'avatar_vip'}} style={{width: 20, height: 20}}/>
                <View style={{flex: 1}}/>
                {/*
                 <Image source={{uri: 'icon_cell_rightarrow'}} style={{width: 20, height: 25}}/>
                 */}
            </View>
            <View style={{flexDirection: 'row', backgroundColor: '#F48365', justifyContent: 'center', padding: 3}}>
                <View style={styles.numsViewStyle}>
                    <Text style={styles.numsViewTextStyle}>10</Text>
                    <Text style={[styles.numsViewTextStyle, {marginTop: 2}]}>抵用券</Text>
                </View>
                <View style={{width: 0.5, backgroundColor: 'white'}}/>
                <View style={styles.numsViewStyle}>
                    <Text style={styles.numsViewTextStyle}>20</Text>
                    <Text style={[styles.numsViewTextStyle, {marginTop: 2}]}>评价</Text>
                </View>
                <View style={{width: 0.5, backgroundColor: 'white'}}/>
                <View style={styles.numsViewStyle}>
                    <Text style={styles.numsViewTextStyle}>34</Text>
                    <Text style={[styles.numsViewTextStyle, {marginTop: 2}]}>收藏</Text>
                </View>
            </View>
        </View>);
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    orderimageStyle: {
        width: 27,
        height: 23
    },
    orderTextStyle: {
        fontSize: 14,
        marginTop: 5,
        color: 'gray',
    },
    orderViewStyle: {
        margin: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headIconStyle: {
        width: 60,
        height: 60,
    },
    headUserNameStyle: {
        color: 'white',
        fontSize: 18,
        marginRight: 5,
        marginLeft: 10
    },
    numsViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    numsViewTextStyle: {
        fontSize: 13,
        color: 'white',
    }

});

//输出组件类
module.exports = Mine;

