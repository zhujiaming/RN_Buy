/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ToastAndroid,
    ScrollView
} from 'react-native';

var TitleBar = require('../kit/TitleBar');
var CommonCell = require('../kit/XMGCommonCell');

var More = React.createClass({
    render() {
        console.log(window);
        return (
            <View style={{flex: 1, backgroundColor: '#F0EFF5'}}>
                {/*导航条*/}
                <TitleBar leftIconUri={'none'} rightIconUri={'icon_mine_setting'} middleText={'更多'}
                          onRightPress={()=> {
                              ToastAndroid.show("这里是更多", ToastAndroid.SHORT)
                          }}/>
                <ScrollView style={styles.container}>
                    <View style={{height: 10}}/>
                    <CommonCell title={'扫一扫'} showBottomLine={true} showTopLine={true}/>
                    <View style={{height: 20}}/>
                    <CommonCell title={'省流量模式'} isSwitch={true} showBottomLine={true} showTopLine={true}/>
                    <CommonCell title={'消息提示'} showBottomLine={true}/>
                    <CommonCell title={'邀请好友'} showBottomLine={true}/>
                    <CommonCell title={'清空缓存'} showBottomLine={true} rightTextHint={'18.5M'}/>
                    <View style={{height: 20}}/>
                    <CommonCell title={'意见反馈'} showBottomLine={true} onCellPress={()=> {
                        ToastAndroid.show('意见反馈', ToastAndroid.SHORT)
                    }}/>
                    <CommonCell title={'问卷调查'} showBottomLine={true}/>
                    <CommonCell title={'支付帮助'} showBottomLine={true}/>
                    <CommonCell title={'网络诊断'} showBottomLine={true}/>
                    <CommonCell title={'关于我们'} showBottomLine={true}/>
                    <View style={{height: 20}}/>
                    <CommonCell title={'精品应用'} showBottomLine={true}/>
                    <View style={{height: 10}}/>
                </ScrollView>
            </View>
        );
    }
});

const styles = StyleSheet.create({
    container: {},


});

//输出组件类
module.exports = More;

