/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';

var Main = require('./Main/XMGMain');

var Launch = React.createClass({
    render() {
        return (
            <Image source={{uri: 'launchimage'}} style={styles.launchImageStyle}/>
        );
    },
    //定时器 网络请求
    componentDidMount(){
        setTimeout(()=> {
            //页面的切换
            this.props.navigator.replace({
                component: Main,//具体路由的版块
            });

        }, 500);
    }
});

const styles = StyleSheet.create({
    launchImageStyle: {
        flex: 1,
    },


});

//输出组件类
module.exports = Launch;

