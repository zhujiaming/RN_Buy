/**
 * Created by Administrator on 2017/3/6.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';

var MiddleComView = require('./XMGMiddleCommenView');
var TopMiddleData = require('../../LocalData/HomeTopMiddleLeft');
var width = require('Dimensions').get('window').width;

var MiddleView = React.createClass({
    render() {
        return (
            <View style={styles.container}>
                {/*左边*/}
                {this._renderLeftView()}
                <View style={{width: 0.5, backgroundColor: '#F0EFF5'}}/>
                {/*右边*/}
                <View style={styles.rightViewStyle}>
                    {this._renderRightView()}
                </View>
            </View>
        );
    },
    _renderLeftView(){
        // return (<Text style={styles.leftViewStyle}>哈哈</Text>)
        var dataLeft = TopMiddleData.dataLeft[0];
        return (<View style={styles.leftViewStyle}>
            <Image source={{uri: dataLeft.img1}} style={{height: 30, width: 100}}/>
            <Image source={{uri: dataLeft.img2}} style={{height: 45, width: 95}}/>
            <Text style={{marginTop: 3}}>{dataLeft.title}</Text>
            <View style={{flexDirection: 'row', marginTop: 3}}>
                <Text style={{color: '#03D2BE', fontSize: 14}}>{dataLeft.price}</Text>
                <Text style={{
                    color: '#FF6100',
                    borderRadius: 8,
                    backgroundColor: 'yellow',
                    fontSize: 12,
                    padding: 2,
                    textAlignVertical: 'center',
                    textAlign: 'center',
                    marginLeft: 2
                }}>{dataLeft.sale}</Text>
            </View>
        </View>);
    },
    _renderRightView(){
        var dataRight = TopMiddleData.dataRight;
        var rightView = [];
        for (var i = 0; i < dataRight.length; i++) {
            rightView.push(<MiddleComView key={i} title={dataRight[i].title}
                                          subTitle={dataRight[i].subTitle}
                                          rightImage={dataRight[i].rightImage}
                                          titleColor={dataRight[i].titleColor} style_={{height:70}}/>);
            if (i != dataRight.length - 1) {
                rightView.push(<View key={":" + i} style={{height: 0.5, backgroundColor: '#F0EFF5'}}/>);
            }
        }
        return rightView;
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 140,
        backgroundColor: 'white',
        flexDirection: 'row',
        marginTop: 20,
    },
    rightViewStyle: {

        flex: 1,
    },
    leftViewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

});

//输出组件类
module.exports = MiddleView;

