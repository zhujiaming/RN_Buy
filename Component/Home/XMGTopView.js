/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ToastAndroid,
    Image,
    Switch,
    TouchableOpacity,
    ScrollView,
    ListView
} from 'react-native';

var {width, height} = require('Dimensions').get('window');
var TopListView = require("./XMGTopListView");

var topMenu = require('../../LocalData/TopMenu.json');

var TopView = React.createClass({
    getDefaultProps(){
        return {};
    },
    getInitialState(){
        return {
            activePage: 0,
        }
    },
    render() {
        return (<View style={styles.container}>
                <ScrollView horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            pagingEnabled={true}
                            onMomentumScrollEnd={(e)=>this._onMomentumScrollEnd(e)}>
                    {this.renderScrollItem()}
                </ScrollView>
                {/*页码部分*/}
                <View style={styles.topIndicatorStyle}>
                    {this._renderIndicator()}
                </View>
            </View>
        );
    },
    renderScrollItem(){
        //组件数组
        var itemArr = [];
        //颜色数组
        var dataArr = topMenu.data;
        //遍历创建
        for (var i = 0; i < dataArr.length; i++) {
            itemArr.push(
                <TopListView key={i} dataArr={dataArr[i]}/>
            );
        }
        return itemArr;
    },
    _renderIndicator(){
        //指示器数组
        var indicatorArr = [], style;
        for (var i = 0; i < 2; i++) {
            //设置圆点样式
            style = (i === this.state.activePage) ? {color: '#FF6100'} : {color: '#e2e2e2'};
            indicatorArr.push(<Text style={[style, {fontSize: 25, margin: 2}]} key={i}>&bull;</Text>)
        }
        return indicatorArr;
    },
    _onMomentumScrollEnd(e){
        var currentPage = Math.floor(e.nativeEvent.contentOffset.x / width);

        this.setState({
            activePage: currentPage,
        })
        ;
    }
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F0EFF5',
    },
    topIndicatorStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        height: 20
    }

});

//输出组件类
module.exports = TopView;

