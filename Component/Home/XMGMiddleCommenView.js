/**
 * Created by Administrator on 2017/3/6.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
var width = require('Dimensions').get('window').width;
var MiddleComView = React.createClass({
    getDefaultProps(){
        return {
            title: '',
            subTitle: '',
            rightImage: '',
            titleColor: '',
            style_: {},
            marginRight: 0,
            marginBottom: 0,
        };
    },
    render() {
        return (
            <View style={[styles.container, this.props.style_, {

                marginBottom: this.props.marginBottom
            }]}>
                {/*左边*/}
                {this._renderLeft()}
                {/*右边*/}
                {this._renderRight()}
                <View style={{backgroundColor: '#F0EFF5', width: this.props.marginRight}}/>
            </View>
        );
    },
    _renderLeft(){
        return (<View style={styles.leftViewStyle}>
            <Text style={{color: this.props.titleColor, fontSize: 14}}>{this.props.title}</Text>
            <Text style={{color: 'gray', fontSize: 11}}>{this.props.subTitle}</Text>
        </View>);
    },
    _renderRight(){
        return (<View style={styles.rightViewStyle}>
            <Image source={{uri: this.props.rightImage}} style={{width: 80, height: 60, resizeMode: 'contain'}}/>
        </View>);
    }
});

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    leftViewStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rightViewStyle: {
        flex: 0.8,
        alignItems: 'center',
        justifyContent: 'center',
    }


});

//输出组件类
module.exports = MiddleComView;

