/**
 * Created by zhujiaming on 17/3/7.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ListView,
    TouchableOpacity,
    Image
} from 'react-native';

var BottomTitleCommView = require('./XMGBottomTitleCommView')

var GuessYouLikeView = React.createClass({
    getDefaultProps(){
        return ({
            api_url: 'http://api.meituan.com/group/v2/recommend/homepage/city/20?userId=160495643&userid=160495643&__vhost=api.mobile.meituan.com&position=23.134643%2C113.373951&movieBundleVersion=100&utm_term=6.6&limit=40&wifi-mac=64%3A09%3A80%3A10%3A15%3A27&ci=20&__skcy=X6Jxu5SCaijU80yX5ioQuvCDKj4%3D&__skua=5657981d60b5e2d83e9c64b453063ada&__skts=1459731016.350255&wifi-name=Xiaomi_1526&client=iphone&uuid=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&__skno=FEB757F5-6120-49EC-85B0-D1444A2C2E7B&utm_content=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&utm_source=AppStore&utm_medium=iphone&version_name=6.6&wifi-cur=0&wifi-strength=&offset=0&utm_campaign=AgroupBgroupD100H0&__skck=3c0cf64e4b039997339ed8fec4cddf05&msid=0FA91DDF-BF5B-4DA2-B05D-FA2032F30C6C2016-04-04-08-38594'
        })
    },
    getInitialState(){
        return ({
            dataSource: new ListView.DataSource({rowHasChanged: (r1, r2)=>r1 !== r2})
        });
    },
    render() {
        return (
            <View style={styles.container}>
                {/*上面标题栏*/}
                <BottomTitleCommView
                    leftIconUri={'cnxh'}
                    title={'猜你喜欢'}
                    rightHint={''}
                />
                <View style={{height: 0.1}}/>
                {/*下面的listView*/}
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow}
                />
            </View>
        );
    },
    componentDidMount(){
        this._loadDataFromNet();
    },
    _renderRow(rowData){
        var imgUrl = rowData.imageUrl;
        imgUrl = imgUrl.replace('w.h', '200.180');
        return (<TouchableOpacity activeOpacity={0.7} onPress={()=> {
        }} style={styles.itemStyle}>
            <Image source={{uri: imgUrl}} style={styles.itemImgLeftStyle}/>
            <View style={styles.itemRightStyle}>
                <View style={{flex: 1.2, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.itemTitleStyle}>{rowData.title}</Text>
                    <View style={{flex: 1}}/>
                    <Text style={styles.itemTitleRightStyle}>{rowData.topRightInfo}</Text>
                </View>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <Text style={styles.itemSubTitleStyle}>{rowData.subTitle}</Text>
                    <View style={{flex: 1}}/>
                </View>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.itemPriceStyle}>{rowData.mainMessage + rowData.mainMessage2}</Text>
                    <View style={{flex: 1}}/>
                    <Text style={styles.itemBottomRightStyle}>{rowData.bottomRightInfo}</Text>
                </View>
            </View>
        </TouchableOpacity>);
    },
    _loadDataFromNet(){
        fetch(this.props.api_url)
            .then((response)=>response.json())
            .then((responseData)=> {
                // console.log(responseData);
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseData.data),
                });
            });
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
    },
    itemStyle: {
        marginTop: 0.5,
        height: 100,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
        backgroundColor: 'white',

    },
    itemImgLeftStyle: {
        width: 100,
        height: 75,
        borderRadius: 7,

    },
    itemRightStyle: {
        flex: 1,
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    itemTitleStyle: {
        color: 'black',
        fontSize: 16,
    },
    itemTitleRightStyle: {
        marginRight: 5,
        fontSize: 13,
    },
    itemSubTitleStyle: {
        fontSize: 13,
        color: 'gray'
    },
    itemPriceStyle: {
        fontSize: 15,
        color: 'red'
    },
    itemBottomRightStyle: {
        fontSize: 13,
        color: 'gray',
        marginRight: 5
    }

});

//输出组件类
module.exports = GuessYouLikeView;

