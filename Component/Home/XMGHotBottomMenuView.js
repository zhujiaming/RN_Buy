/**
 * Created by zhujiaming on 17/3/7.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';
var width = require('Dimensions').get('window').width;


var HotBottomMenu = React.createClass({
    getDefaultProps(){
        return ({
            title: '',
            subTitle: '',
            hotImage: '',
        });
    },
    render(){
        return (<View style={{
            width: width / 4,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white',
            paddingTop: 5,
            paddingBottom: 5,
        } }>
            <Text style={{color: 'gray', fontSize: 14}}>{this.props.title}</Text>
            <Text style={{color: 'gray', fontSize: 11}}>{this.props.subTitle}</Text>
            <Image source={{uri: this.props.hotImage}} style={{width: 80, height: 50, resizeMode: 'contain'}}/>
        </View>);
    }
});

module.exports = HotBottomMenu;