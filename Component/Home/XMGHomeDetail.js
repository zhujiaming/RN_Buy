/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ToastAndroid,
    WebView
} from 'react-native';

var TitleBar = require('../kit/TitleBar')

var HomeDetail = React.createClass({

    getInitialState(){
        return {
            // url: this.props.rowData.detailurl + '?uuid=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&utm_term=6.6&utm_source=AppStore&utm_content=5C7B6342814C7B496D836A69C872202B5DE8DB689A2D777DFC717E10FC0B4271&version_name=6.6&userid=160495643&utm_medium=iphone&lat=23.134709&utm_campaign=AgroupBgroupD100Ghomepage_shoppingmall_detailH0&token=b81UqRVf6pTL4UPLLBU7onkvyQoAAAAAAQIAACQVmmlv_Qf_xR-hBJVMtIlq7nYgStcvRiK_CHFmZ5Gf70DR47KP2VSP1Fu5Fc1ndA&lng=113.373890&f=iphone&ci=20&msid=0FA91DDF-BF5B-4DA2-B05D-FA2032F30C6C2016-04-04-08-38594',
            url: 'http://m.shijiebang.com/?sr=gp_4235293256',
        }
    },
    render() {
        return (
            <View style={styles.container}>
                <TitleBar middleText={this.props.rowData.name} navigator={this.props.navigator}/>
                <View style={styles.contentStyle}>
                    <WebView
                        automaticallyAdjustContentInsets={false}
                        style={styles.webView}
                        source={{uri: this.state.url}}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        decelerationRate="normal"
                        startInLoadingState={true}
                        onMessage={this.onMessage}
                        onNavigationStateChange={this.onNavigationStateChange}
                    />

                </View>
            </View>
        );
    },
    componentDidMount(){
        // ToastAndroid.show(this.props.rowData.promotionText, ToastAndroid.SHORT);

    },
    onNavigationStateChange(event){
        console.log('onNavigationStateChange->event:');
        console.log(event); //打印出event中属性
    },
    onMessage(event){
        console.log('onMessage->event.nativeEvent.data:');
        console.log(event.nativeEvent.data);
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentStyle: {
        backgroundColor: 'white',
        flex: 1
    }


});

//输出组件类
module.exports = HomeDetail;

