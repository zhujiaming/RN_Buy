/**
 * Created by Administrator on 2017/3/7.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ListView,
    Image,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
var {width, height} = require('Dimensions').get('window');

var BottomTitleCommView = require('./XMGBottomTitleCommView');

var HomeDetail = require('./XMGHomeDetail')

var BottomHorScrollView = React.createClass({
    getDefaultProps(){
        return {dataArr: []}
    },
    getInitialState(){
        return {
            dataSource: new ListView
                .DataSource({rowHasChanged: (r1, r2)=>r1 !== r2})
                .cloneWithRows(this.props.dataArr)
        }
    },
    render() {
        return (
            <View style={styles.container}>
                {/*上面标题栏*/}
                <BottomTitleCommView
                    leftIconUri={'gw'}
                    title={'购物中心'}
                    rightHint={'查看全部'}
                />
                <View style={{height: 0.2}}/>
                {/*下面横向滑动*/}
                <ListView
                    contentContainerStyle={styles.lvStyle}
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        );
    },
    _renderRow(rowData){
        return (<View style={[styles.itemStyle, {width: this._getItemWidth(), height: this._getItemheight()}]}>
            <TouchableOpacity activeOpacity={0.7} onPress={()=> this._onItemPress(rowData)}>
                <View>
                    <Image source={{uri: rowData.img}}
                           style={{
                               borderRadius: 6,
                               width: this._getItemWidth() - 20,
                               height: this._getItemheight() - 40
                           }}/>
                    <Text style={{
                        backgroundColor: 'orange',
                        color: 'white',
                        position: 'absolute',
                        bottom: 10,
                        fontSize: 10,
                        padding: 2
                    }}>{rowData.showtext.count}家优惠</Text>
                </View>

                <Text style={{marginTop: 3, color: '#2D2D2D'}}>{rowData.name}</Text>
            </TouchableOpacity>
        </View>)
    },
    _getItemWidth(){
        var w = width * (2 / 5);
        return w;
    },
    _getItemheight(){
        var w = width / 3;
        return w;
    },
    _onItemPress(rowData){
        ToastAndroid.show(rowData.name, ToastAndroid.SHORT);
        rowData.detailurl = rowData.detailurl.replace('imeituan://www.meituan.com/web/?url=', '');
        this.props.navigator.push({
            component: HomeDetail,
            params: {
                rowData: rowData,
            }
        });
    }

});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        marginBottom: 20,
    },
    lvStyle: {
        // flexDirection: 'row',
        marginTop: 1,
        backgroundColor: 'white',
    },
    itemStyle: {
        width: width / 3,
        height: width / 3,
        alignItems: 'center',
        justifyContent: 'center'
    }


});

//输出组件类
module.exports = BottomHorScrollView;