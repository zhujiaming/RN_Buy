/**
 * Created by zhujiaming on 17/3/8.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    ToastAndroid
} from 'react-native';

var XMGHomeDetail = require('./XMGHomeDetail');

var HomeTitleView = React.createClass({
    render() {
        return (<View style={styles.topBarStyle}>
            {/*左边*/}
            <TouchableOpacity activeOpacity={0.5} onPress={()=> {
                ToastAndroid.show('选择地区', ToastAndroid.SHORT)
                this.props.navigator.push({
                    component: XMGHomeDetail
                });
            }}>
                <Text style={styles.topLeftTextStyle}>广州</Text>
            </TouchableOpacity>
            {/*中间*/}
            <TextInput
                placeholder='输入商家，品类，商圈'
                placeholderTextColor='gray'
                style={styles.topInputStyle}
                selectionColor='#FF6100'
                underlineColorAndroid='#00000000'
            />
            {/*右边*/}
            <View style={styles.rightViewStyle}>
                <TouchableOpacity activeOpacity={0.5} onPress={()=> {
                    ToastAndroid.show('消息', ToastAndroid.SHORT)
                }}>
                    <Image source={{uri: 'icon_homepage_message'}} style={styles.topBarRightImgStyle}/>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.5} onPress={()=> {
                    ToastAndroid.show('扫码', ToastAndroid.SHORT)
                }}>
                    <Image source={{uri: 'icon_homepage_scan'}} style={[styles.topBarRightImgStyle, {marginLeft: 5}]}/>
                </TouchableOpacity>
            </View>
        </View>);
    }
});

const styles = StyleSheet.create({

    topInputStyle: {
        color: 'gray',
        borderRadius: 20,
        backgroundColor: 'white',
        flex: 1,
        margin: 10,
        padding: 0,
        paddingLeft: 10,
        paddingRight: 10,
        textAlignVertical: 'center',
        textAlign: 'center'
    },
    topBarStyle: {
        flexDirection: 'row',
        backgroundColor: '#FF6100',
        alignItems: 'center',
        height: 49,
        paddingLeft: 5,
        paddingRight: 5,
    },
    topLeftTextStyle: {
        color: 'white',
        fontSize: 16,
    },
    rightViewStyle: {
        flexDirection: 'row'
    },
    topBarRightImgStyle: {
        width: 23,
        height: 23,
    }


});

//输出组件类
module.exports = HomeTitleView;

