/**
 * Created by Administrator on 2017/3/7.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';


var BottomTitleCommView = React.createClass({
        getDefaultProps(){
            return {
                leftIconUri: '',
                title: '',
                rightHint: '',
            }
        },
        render()
        {
            return (
                <View style={styles.container}>
                    <Image style={{width: 30, height: 30, marginLeft: 10}} source={{uri: this.props.leftIconUri}}/>
                    <Text style={styles.titleStyle}>{this.props.title}</Text>
                    <View style={{flex: 1}}/>
                    <Text style={styles.hintStyle}>{this.props.rightHint}</Text>
                    <Image style={{width: 20, height: 30, marginRight: 10}} source={{uri: 'home_arrow'}}/>
                </View>
            );
        }
    })
    ;

const styles = StyleSheet.create({
    container: {
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
    },
    titleStyle: {
        marginLeft: 5,
        fontSize: 16,
        color: '#2D2D2D',
    },
    hintStyle: {
        color: 'gray',
        fontSize: 14,
        marginRight: 10,
    }


});

//输出组件类
module.exports = BottomTitleCommView;