/**
 * Created by Administrator on 2017/3/6.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
var MiddleComView = require('./XMGMiddleCommenView');
var width = require('Dimensions').get('window').width;

var MiddleData = require('../../LocalData/XMG_Home_D4.json');


var BottomView = React.createClass({
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topViewStyle}></View>
                <View style={styles.bottomViewStyle}>
                    {this._renderBottomView()}
                </View>
            </View>
        );
    },
    _renderBottomView(){
        var itemArr = [];
        var dataArr = MiddleData.data;
        for (var i = 0; i < dataArr.length; i++) {
            var imgUrl = dataArr[i].imageurl;
            imgUrl = imgUrl.replace('/w.h/', '/50.50/');
            console.log('imgUrl:' + imgUrl);
            itemArr.push(<MiddleComView key={i} title={dataArr[i].maintitle}
                                        subTitle={dataArr[i].deputytitle}
                                        rightImage={imgUrl}
                                        titleColor={dataArr[i].typeface_color}
                                        style_={{width: width * 0.5, height: 70}}
                                        marginRight={i % 2 === 0 ? 1 : 0}
                                        marginBottom={i % 2 === 0 && i != dataArr.length ? 1 : 0}
            />);

        }
        return itemArr;
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
    },
    topViewStyle: {},
    bottomViewStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    }


});

//输出组件类
module.exports = BottomView;