/**
 * Created by Administrator on 2017/3/1.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    ToastAndroid,
    ScrollView
} from 'react-native';

var XMGHomeDetail = require('./XMGHomeDetail');
var HomeTitleView = require('./XMGHomeTitleView');
var TopView = require('./XMGTopView');
var MiddleView = require('./XMGHomeMiddleView');
var MiddleBottomView = require('./XMGMiddleBottomView');
var BottomHorScrollView = require('./XMGBottomHorScrollView');
var HotModuleView = require('./XMGHotModuleView');
var GuessYouLikeView = require('./XMGGuessYouLikeView');

var shopCenterData = require('../../LocalData/XMG_Home_D5.json');

var Home = React.createClass({
    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#F0EFF5'}}>
                <HomeTitleView navigator={this.props.navigator/*这里是将navigator引用传递到子控件*/}
                />
                {/*首页主要内容*/}
                <ScrollView showsVerticalScrollIndicator={false}>
                    {/*头部view*/}
                    <TopView/>
                    {/*中间内容*/}
                    <MiddleView />
                    {/*中下半部分*/}
                    <MiddleBottomView/>
                    {/*中下横向滑动（购物中心）*/}
                    <BottomHorScrollView dataArr={shopCenterData.data} navigator={this.props.navigator}/>
                    {/*热门频道*/}
                    <HotModuleView/>
                    {/*猜你喜欢*/}
                    <GuessYouLikeView/>
                </ScrollView>
            </View>
        );
    },
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F0EFF5'
    },
    topInputStyle: {
        color: 'gray',
        borderRadius: 20,
        backgroundColor: 'white',
        flex: 1,
        margin: 10,
        padding: 0,
        paddingLeft: 10,
        paddingRight: 10,
        textAlignVertical: 'center',
        textAlign: 'center'
    },
    topBarStyle: {
        flexDirection: 'row',
        backgroundColor: '#FF6100',
        alignItems: 'center',
        height: 49,
        paddingLeft: 5,
        paddingRight: 5,
    },
    topLeftTextStyle: {
        color: 'white',
        fontSize: 16,
    },
    rightViewStyle: {
        flexDirection: 'row'
    },
    topBarRightImgStyle: {
        width: 23,
        height: 23,
    }


});

//输出组件类
module.exports = Home;