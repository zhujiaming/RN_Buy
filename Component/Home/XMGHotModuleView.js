/**
 * Created by Administrator on 2017/3/7.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';

var BottomTitleCommView = require('./XMGBottomTitleCommView');
var HotData = require('../../LocalData/HomeHotData.json');
var MiddleComView = require('./XMGMiddleCommenView');
var width = require('Dimensions').get('window').width;
var HotBottomMenu = require('./XMGHotBottomMenuView');

var HotModuleView = React.createClass({
    render() {
        return (
            <View style={styles.container}>
                {/*上面标题栏*/}
                <BottomTitleCommView
                    leftIconUri={'rm'}
                    title={'热门频道'}
                    rightHint={'查看全部'}
                />
                {/*下面菜单部分*/}
                <View style={{flexDirection: 'row', marginTop: 0.3}}>
                    {this._renderTopMenu()}
                </View>

                <View style={{flexDirection: 'row', marginTop: 0.3}}>
                    {this._renderBottomMenu()}
                </View>
            </View>
        );
    },
    _renderTopMenu(){
        var itemArr = [];
        var dataArr = HotData.topData;
        for (var i = 0; i < dataArr.length; i++) {
            itemArr.push(<MiddleComView key={i} title={dataArr[i].title}
                                        subTitle={dataArr[i].subTitle}
                                        rightImage={dataArr[i].hotImage}
                                        titleColor={'gray'}
                                        style_={{width: width * 0.5, height: 70}}
                                        marginRight={i % 2 === 0 ? 1 : 0}
                                        marginBottom={i % 2 === 0 && i != dataArr.length ? 1 : 0}
            />);

        }
        return itemArr;
    },
    _renderBottomMenu(){
        var itemArr = [];
        var dataArr = HotData.bottomData;
        for (var i = 0; i < dataArr.length; i++) {
            itemArr.push(<HotBottomMenu title={dataArr[i].title}
                                        subTitle={dataArr[i].subTitle}
                                        hotImage={dataArr[i].hotImage}
                                        key={i}
            />);
        }
        return itemArr;
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },


});

//输出组件类
module.exports = HotModuleView;

