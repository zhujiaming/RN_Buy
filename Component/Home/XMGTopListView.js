/**
 * Created by Administrator on 2017/3/1.
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ToastAndroid,
    Image,
    Switch,
    TouchableOpacity,
    ListView
} from 'react-native';

var {width, height} = require('Dimensions').get('window');

var TopView = React.createClass({
    getDefaultProps(){
        return {
            dataArr: [],
        };
    },
    getInitialState(){
        //创建数据源
        var ds = new ListView.DataSource({
            rowHasChanged: (r1, r2)=> {
                r1 !== r2
            }
        });
        return {
            dataSource: ds.cloneWithRows(this.props.dataArr),
        }
    },
    render() {
        return (<ListView
            dataSource={this.state.dataSource}
            renderRow={this._renderRow}
            contentContainerStyle={styles.contentViewStyle}
            scrollEnabled={false}
        />);
    },
    //具体的cell
    _renderRow(rowData){
        return (<TouchableOpacity activeOpacity={0.7} style={styles.topItemStyle}
                                  onPress={()=>this._onItemPress(rowData)}><View>
                <Image style={{width: 50, height: 50, alignSelf: 'center'}} source={{uri: rowData.image}}/>
                <Text style={styles.topMenuTextStyle}>{rowData.title}</Text>
            </View></TouchableOpacity>
        );
    },
    _onItemPress(rowData){
        ToastAndroid.show(rowData.title, ToastAndroid.SHORT)
    }
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F0EFF5',
    },
    topItemStyle: {
        // backgroundColor: 'red',
        width: width / 5,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    topMenuTextStyle: {
        fontSize: 13,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    contentViewStyle: {
        //多个cell在同一行显示
        flexWrap: 'wrap',
        //主轴方向
        flexDirection: 'row',
        width: width,
        backgroundColor: 'white',
    }

});

//输出组件类
module.exports = TopView;

