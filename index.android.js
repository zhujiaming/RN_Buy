/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator,
    Image,
} from 'react-native';

var Main = require('./Component/Main/XMGMain.js')
var LaunchImage = require('./Component/XMGLaunchImage');

export default class RN_Buy extends Component {
    render() {
        return (<Navigator
                initialRoute={{name: '启动页', component: LaunchImage}}
                configureScene={(route) => {
                    return Navigator.SceneConfigs.FloatFromRight;
                }}
                renderScene={(route, navigator) => {
                    let Component = route.component;
                    return <Component {...route.params} navigator={navigator}/>
                }}
            />
        );
    }
}

AppRegistry.registerComponent('RN_Buy', () => RN_Buy);
